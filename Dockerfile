FROM sharelatex/sharelatex:2.5.0

WORKDIR /var/www/sharelatex
RUN rm -rf web
ADD ./web ./web

WORKDIR /var/www/sharelatex/web
RUN npm install --quiet
RUN npm run webpack:production

EXPOSE 80

WORKDIR /

ENTRYPOINT ["/sbin/my_init"]
